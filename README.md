# docmd
Toolbox for generating documentation using .md files.


# Known Bugs
In comments char '/' soetimes causes that cdoc cannot find documentation for some items.

It's related to regex:
``` (\/\*[^\/]*?\*\/\s*)? ```
that is used to search for the multiline cdoc comment.

Restructurization of this part: ``` [^\/]*? ``` should fix this problem.

# Build
Build all with single command run:
```
tox
```

Build wheel for specific python:
```
tox -e py310
tox -e py311
```

# Publish
> require tokens for pypi and testpypi in ~/.pypirc file

### publish on pypi
```
tox -e publish
```

### publish on testpypi
```
tox -e publish-test
```
