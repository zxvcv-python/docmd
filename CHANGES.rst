Changelog
=========

0.2.0b1 (2023-02-15)
--------------------
- Python support for version 3.10 3.11.
- Update project configurations and informations.

0.1.20 (2022-11-08)
-------------------
- Fix in DocGenerator.__init__() - resolving self.main_file_output_dir.

0.1.19 (2022-11-05)
-------------------
- Fix for CFuncDecl name (before CFunDecl used).
- CFuncDecl parameter doc generation updates (allow multiline description for @param).
- CStructure parameter doc generation updates (allow commenting only few fields in Structure).

0.1.18 (2022-11-01)
-------------------
- Generating documentation for enums.
- Add class CEnum.
- Add template for enum documentation.

0.1.17 (2022-11-01)
-------------------
- Usage of handle_gnu_specific_symbols parameter (followed by changes in pycparser-util 0.1.8).
- Settings, options and requires will not appear on settings page if empty.
- Fixes.

0.1.16 (2022-10-30)
-------------------
- Fix for api_struct template.

0.1.15 (2022-10-30)
-------------------
- Fix for _get_user_blocks(), now no data is deleted on doc regeneration.
- Add script interface.

0.1.14 (2022-10-29)
-------------------
- Generating documentation for typedefs.
- Add class CTypedef.
- Dont generate api for specific group if it dont exist in the file.
- Add template for typedef documentation.

0.1.13 (2022-10-29)
-------------------
- Add strip_multiline_comment() to CDoc.
- Generating documentation for structures.
- Implementation for CStruct.generate_doc_page().
- Template documentation for Structure.

0.1.12 (2022-10-29)
-------------------
- Improvement fro FuncDecl doc generation.

0.1.11 (2022-10-29)
-------------------
- Separate chapters for Structures, Unions, Enums, Typedefs documentation.
- cdoc_objects (CDocFunDecl, CDocStruct) moved to cdoc_objects file.
- cdoc_objects are now inherited from pycparser-util.c_objects

0.1.10 (2022-10-28)
-------------------
- Fixes: respectively to changes in pycparser-util 0.1.4
- Add class: CDocStruct.py

0.1.9 (2022-10-27)
------------------
- Fixes

0.1.8 (2022-10-26)
------------------
- Code structure improvements.
- Generating examples (examples.md) page.
- Generating bugs (bugs.md) page.
- Generating build_test (build_test.md) page.

0.1.7 (2022-10-26)
------------------
- Generating settings (settings.md) page.

0.1.6 (2022-10-26)
------------------
- Generating main (README.md) page.

0.1.5 (2022-10-25)
------------------
- DocGenerator with ConanDocGenerator for conan packages.
- Restructurization for generate_api_page.
- Add clean generation parameter for generate_api_page.
- Templates api, footer and heading->header updates.

0.1.4 (2022-10-24)
------------------
- Generating documentation for function declarations.

0.1.3 (2022-10-23)
------------------
- First implementation for generating api doc for function declarations.

0.1.2 (2022-10-22)
------------------
- Implementation updates - searching for function declarations.

0.1.1 (2022-10-10)
------------------
- Basic test main template generation.

0.1.0 (2022-10-10)
------------------
- Initial.
