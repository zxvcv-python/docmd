import re
import os
import sys

from pathlib import Path
from typing import List, Dict, Optional
from Cheetah.Template import Template

from pycparser_util.CParserUtil import CParserUtil
from pycparser_util.c_ast_getters import StructGetter, FuncDeclGetter, TypedefGetter, EnumGetter

from .CDoc import CDoc
from .cdoc_objects import CFuncDecl, CStruct, CTypedef, CEnum


class DocGenerator(CDoc):
    @classmethod
    def gather_project_info(cls, name:str, description:str, repository_url:str, version:str,
                                 license:str, author:str):
        cls.project_info = {}
        cls.project_info["name"] = name
        cls.project_info["description"] = description
        cls.project_info["repository_url"] = repository_url
        cls.project_info["version"] = version
        cls.project_info["license"] = license
        cls.project_info["author"] = author

        cls.project_info["conan_package_doc"] = False

    def __init__(self, main_file_output_dir:Path.cwd(), output_subdir:Path=Path("docs")):
        self.main_file_output_dir = main_file_output_dir.resolve()
        self.main_file_output_dir.mkdir(parents=True, exist_ok=True)

        self.output_subdir = (self.main_file_output_dir/output_subdir).resolve()
        self.output_subdir.mkdir(parents=True, exist_ok=True)

    def _get_header(self) -> str:
        return str(Template(
            self.load_template("header"),
            [
                self.project_info
            ]
        ))

    def _get_footer(self, repository_url:str, main_page:str) -> str:
        return str(Template(
            self.load_template("footer"),
            [
                self.project_info,
                {
                    "repository_url": repository_url,
                    "main_page": main_page
                }
            ]
        ))

    def _get_user_blocks(self, file_content:str, blocks_count:int, clean:bool):
        matches = []
        if not clean:
            for i in range(blocks_count):
                match = re.search(
                    re.compile("".join([
                        f"\[comment\]: \# \(\[\[_DOCMD_USER_BLOCK_{i}_BEGIN\]\]\)",
                        r"(.|\n)*",
                        f"\[comment\]: \# \(\[\[_DOCMD_USER_BLOCK_{i}_END\]\]\)"
                    ])),
                    file_content
                )

                if match:
                    matches.append(match.group())

            matches = ["\n".join(m.splitlines()[1:-1]) for m in matches]

        block_match = re.findall(
            re.compile("_DOCMD_USER_BLOCK_\d_BEGIN"),
            file_content
        )

        blocks = {}
        for match in zip(block_match, matches):
            block_number = int(match[0].split("_")[4])

            blocks[block_number] = match[1]

        counter = 0
        user_blocks = {}
        for i in range(blocks_count):
            if i in blocks:
                user_blocks[f"_DOCMD_USER_BLOCK_{i}"] = "" if clean else matches[counter]
                counter = counter + 1
            else:
                user_blocks[f"_DOCMD_USER_BLOCK_{i}"] = ""
        return user_blocks

    def _generate_page(self, template_variables:List[dict], output_file:Path, template_name:str,
                             user_blocks_count:int, clean:bool):
        try:
            with output_file.open("r") as f:
                file_content = f.read()
        except FileNotFoundError as exc:
            print(f"File {output_file.name} could not be found, generating as 'clean'.")
            clean = True
            file_content = ""

        user_blocks = self._get_user_blocks(file_content, user_blocks_count, clean)
        template_variables.append(self.project_info)
        template_variables.append(user_blocks)

        content = str(Template(
            self.load_template(template_name),
            template_variables
        ))

        with output_file.open("w+") as f:
            f.write(str(content))

    def generate_api_page(self, files:List[Path],
                                includes:List[Path] = [],
                                defines:Dict[str, Optional[str]] = {},
                                compiler_params:List[str] = [],
                                clean:bool = True):
        # enum declarations
        enums_doc_list = []
        for item in CParserUtil.find_items(EnumGetter, CEnum,
            files, includes, defines, compiler_params, skip_included=True,
            handle_gnu_specific_symbols=True):
            try:
                enums_doc_list.append(item.generate_doc_page())
            except ValueError as exc:
                print(f"CDoc parsing error for Enum {item.name}")

        # stucture declarations
        structures_doc_list = []
        for item in CParserUtil.find_items(StructGetter, CStruct,
            files, includes, defines, compiler_params, skip_included=True,
            handle_gnu_specific_symbols=True):
            try:
                structures_doc_list.append(item.generate_doc_page())
            except ValueError as exc:
                print(f"CDoc parsing error for Struct {item.name}")

        # typedef declarations
        typedef_doc_list = []
        for item in CParserUtil.find_items(TypedefGetter, CTypedef,
            files, includes, defines, compiler_params, skip_included=True,
            handle_gnu_specific_symbols=True):
            try:
                typedef_doc_list.append(item.generate_doc_page())
            except ValueError as exc:
                print(f"CDoc parsing error for Typedef {item.name}")

        # function declarations
        functions_doc_list = []
        for item in CParserUtil.find_items(FuncDeclGetter, CFuncDecl,
            files, includes, defines, compiler_params, skip_included=True,
            handle_gnu_specific_symbols=True):
            try:
                functions_doc_list.append(item.generate_doc_page())
            except ValueError as exc:
                print(f"CDoc parsing error for function {item.name}")

        self._generate_page(
            template_variables=[{
                "structures": "".join([fd for fd in structures_doc_list]),
                "unions": "",
                "enums": "".join([fd for fd in enums_doc_list]),
                "typedefs": "".join([fd for fd in typedef_doc_list]),
                "functions": "".join([fd for fd in functions_doc_list]),
                "header": self._get_header(),
                "footer": self._get_footer(
                    repository_url=self.project_info["repository_url"],
                    main_page=str(Path(os.path.relpath(self.main_file_output_dir, self.output_subdir))/"README.md")
                )
            }],
            output_file=self.output_subdir/"api.md",
            template_name="api",
            user_blocks_count=7,
            clean=clean
        )

    def generate_build_test_page(self, clean:bool = True):
        self._generate_page(
            template_variables=[{
                "header": self._get_header(),
                "footer": self._get_footer(
                    repository_url=self.project_info["repository_url"],
                    main_page=str(Path(os.path.relpath(self.main_file_output_dir, self.output_subdir))/"README.md")
                )
            }],
            output_file=self.output_subdir/"build_test.md",
            template_name="build_test",
            user_blocks_count=4,
            clean=clean
        )

    def generate_examples_page(self, clean:bool = True):
        self._generate_page(
            template_variables=[{
                "header": self._get_header(),
                "footer": self._get_footer(
                    repository_url=self.project_info["repository_url"],
                    main_page=str(Path(os.path.relpath(self.main_file_output_dir, self.output_subdir))/"README.md")
                )
            }],
            output_file=self.output_subdir/"examples.md",
            template_name="examples",
            user_blocks_count=2,
            clean=clean
        )

    def generate_bugs_page(self, clean:bool = True):
        self._generate_page(
            template_variables=[{
                "header": self._get_header(),
                "footer": self._get_footer(
                    repository_url=self.project_info["repository_url"],
                    main_page=str(Path(os.path.relpath(self.main_file_output_dir, self.output_subdir))/"README.md")
                )
            }],
            output_file=self.output_subdir/"bugs.md",
            template_name="bugs",
            user_blocks_count=2,
            clean=clean
        )

    def generate_main(self, clean:bool = True):
        DOCMD_USER_BLOCKS_COUNT = 4

        try:
            with (self.main_file_output_dir/"README.md").open("r") as f:
                file_content = f.read()
        except FileNotFoundError as exc:
            print("File README.md could not be found, generating as 'clean'.")
            clean = True
            file_content = ""

        user_blocks = self._get_user_blocks(file_content, DOCMD_USER_BLOCKS_COUNT, clean)

        content = str(Template(
            self.load_template("main"),
            [
                self.project_info,
                {
                    "docs_dir": str(self.output_subdir.relative_to(self.main_file_output_dir)),
                    "header": self._get_header(),
                    "footer": self._get_footer(
                        repository_url=self.project_info["repository_url"],
                        main_page=str("./README.md")
                    )
                },
                user_blocks
            ]
        ))

        with (self.main_file_output_dir/"README.md").open("w+") as f:
            f.write(str(content))

    def generate(self, files:List[Path],
                       includes:List[Path] = [],
                       defines:Dict[str, Optional[str]] = {},
                       compiler_params:List[str] = [],
                       clean:bool = True):
        self.generate_main(
            clean=clean
        )
        self.generate_api_page(
            files=files,
            includes=includes,
            defines=defines,
            compiler_params=compiler_params,
            clean=clean
        )
        self.generate_build_test_page(
            clean=clean
        )
        self.generate_examples_page(
            clean=clean
        )
        self.generate_bugs_page(
            clean=clean
        )

class ConanDocGenerator(DocGenerator):
    def __init__(self, main_file_output_dir:Path.cwd(), output_subdir:Path=Path("docs"),
                       workspace:Path=Path.cwd()):
        super().__init__(main_file_output_dir, output_subdir)

        # import ConanPackage class from conanfile.py file
        conanfile_file = (workspace/"conanfile.py").resolve()
        sys.path.append(str(conanfile_file.parent))
        from conanfile import ConanPackage

        ConanDocGenerator.gather_project_info(
            name=ConanPackage.name.strip(),
            description=ConanPackage.description.strip(),
            repository_url=ConanPackage.url.strip(),
            version=ConanPackage.version.strip(),
            license=ConanPackage.license.strip(),
            author=ConanPackage.author.strip()
        )

        self.project_info["conan_package_doc"] = True

    def generate_settings_page(self, clean:bool = True):
        from conanfile import ConanPackage

        options = []
        if ConanPackage.options:
            for name, value in ConanPackage.options.items():
                options.append({
                    "name": name,
                    "values": value,
                    "default": ConanPackage.default_options[name]
                })

        self._generate_page(
            template_variables=[{
                "settings": ConanPackage.settings,
                "options": options,
                "requires": ConanPackage.requires if hasattr(ConanPackage, "requires") else "",
                "header": self._get_header(),
                "footer": self._get_footer(
                    repository_url=self.project_info["repository_url"],
                    main_page=str(Path(os.path.relpath(self.main_file_output_dir, self.output_subdir))/"README.md")
                )
            }],
            output_file=self.output_subdir/"settings.md",
            template_name="settings",
            user_blocks_count=3,
            clean=clean
        )

    def generate(self, files:List[Path],
                       includes:List[Path] = [],
                       defines:Dict[str, Optional[str]] = {},
                       compiler_params:List[str] = [],
                       clean:bool = True):
        super().generate(
            files=files,
            includes=includes,
            defines=defines,
            compiler_params=compiler_params,
            clean=clean
        )
        self.generate_settings_page(
            clean=clean
        )
