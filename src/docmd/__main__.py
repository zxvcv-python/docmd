import sys
import logging
import argparse
from typing import List
from pathlib import Path

from zxvcv.util import ArgsManager, LogManager

from .DocGenerator import ConanDocGenerator

LOGGER_NAME = __package__
log = logging.getLogger(LOGGER_NAME)


class LocalArgsManager(ArgsManager):
    class ParseFilesArgument(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            values = [Path(v).resolve() for v in values]
            setattr(namespace, self.dest, values)

    class ParseIncludesArgument(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            values = [Path(v).resolve() for v in values]
            setattr(namespace, self.dest, values)

    class ParseDefinesArgument(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            defines = {}
            for d in values:
                ds = d.split("=")
                defines[ds[0]] = None if len(ds) == 1 else ds[1]
            setattr(namespace, self.dest, defines)

    def _add_generate_subcommand(self, parser):
        command_parser = parser.add_parser("generate",
            help="Generate documentation for passed files."
        )

        compiler_group = command_parser.add_argument_group("basic arguments")
        compiler_group.add_argument("files",
            action=LocalArgsManager.ParseFilesArgument,
            nargs="*",
            default=[],
            help="""Files for which documentation will be generated."""
        )

        compiler_group = command_parser.add_argument_group("compiler arguments")
        compiler_group.add_argument("--includes",
            type=str,
            action=LocalArgsManager.ParseIncludesArgument,
            nargs="*",
            default=[],
            help="""Include paths to be passed as parameter '-I<path>' into the compiler.
                    Separated by spaces."""
        )
        compiler_group.add_argument("--defines",
            type=str,
            action=LocalArgsManager.ParseDefinesArgument,
            nargs="*",
            default={},
            help="""Definitions to be passed as parameter '-D<definiton>' into the compiler.
                    Separated by spaces."""
        )
        compiler_group.add_argument("--compiler-params",
            type=str,
            nargs="*",
            default=[],
            dest="compiler_params",
            help="""Other custom parameters to be passed into the compiler.
                    Separated by spaces."""
        )

        generate_group = command_parser.add_argument_group("generate arguments")
        generate_group.add_argument("--workplace", type=Path,
            default=Path.cwd(),
            help="""Workplace where project files are stored."""
        )
        generate_group.add_argument("--output", type=Path,
            default=Path.cwd(),
            help="""Set output directory for documentation files."""
        )
        generate_group.add_argument("--output-subdir", type=Path,
            default=Path("docs"),
            dest="output_subdir",
            help="""If specified, only main README.md will be placed in 'output' direcotry.
                    Other files will be placed in output-subdir, and all doc links will be
                    generated respectively.
                    Default to 'docs'."""
        )
        generate_group.add_argument("--clean",
            action="store_true",
            help="""If selected, no user changes will be kept.
                    Documentation will be generated form scratch."""
        )

        super()._add_logging_arguments(command_parser)

    def get_parser(self) -> argparse.ArgumentParser:
        self.subparsers = self.parser.add_subparsers(required=True, dest="action")
        self._add_generate_subcommand(self.subparsers)
        return self.parser

def main(argv=sys.argv[1:]):
    args = LocalArgsManager("docmd").get_parser().parse_args(argv)
    LogManager.setup_logger(LOGGER_NAME, level=int(args.log))
    log.debug(f"Script arguments: {args}")

    if args.action == "generate":
        docg = ConanDocGenerator(args.output, args.output_subdir, args.workplace)
        docg.generate(
            files=args.files,
            includes=args.includes,
            defines=args.defines,
            compiler_params=args.compiler_params,
            clean=args.clean
        )

if __name__.rpartition(".")[-1] == "__main__":
    sys.exit(main(sys.argv[1:]))
