$header

\# API

[comment]: \# ([[_DOCMD_USER_BLOCK_0_BEGIN]])
${_DOCMD_USER_BLOCK_0}
[comment]: \# ([[_DOCMD_USER_BLOCK_0_END]])

</br>

#if $structures != ""
\#\# Structures
***

[comment]: \# ([[_DOCMD_USER_BLOCK_1_BEGIN]])
${_DOCMD_USER_BLOCK_1}
[comment]: \# ([[_DOCMD_USER_BLOCK_1_END]])

$structures
#end if

#if $unions != ""
\#\# Unions
***

[comment]: \# ([[_DOCMD_USER_BLOCK_2_BEGIN]])
${_DOCMD_USER_BLOCK_2}
[comment]: \# ([[_DOCMD_USER_BLOCK_2_END]])

$unions
#end if

#if $enums != ""
\#\# Enums
***

[comment]: \# ([[_DOCMD_USER_BLOCK_3_BEGIN]])
${_DOCMD_USER_BLOCK_3}
[comment]: \# ([[_DOCMD_USER_BLOCK_3_END]])

$enums
#end if

#if $typedefs != ""
\#\# Typedefs
***

[comment]: \# ([[_DOCMD_USER_BLOCK_4_BEGIN]])
${_DOCMD_USER_BLOCK_4}
[comment]: \# ([[_DOCMD_USER_BLOCK_4_END]])

$typedefs
#end if

</br>

#if $functions != ""
\#\# Functions
***

[comment]: \# ([[_DOCMD_USER_BLOCK_5_BEGIN]])
${_DOCMD_USER_BLOCK_5}
[comment]: \# ([[_DOCMD_USER_BLOCK_5_END]])

$functions

[comment]: \# ([[_DOCMD_USER_BLOCK_6_BEGIN]])
${_DOCMD_USER_BLOCK_6}
[comment]: \# ([[_DOCMD_USER_BLOCK_6_END]])
#end if

$footer