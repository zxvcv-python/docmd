$header

\# Building

[comment]: \# ([[_DOCMD_USER_BLOCK_0_BEGIN]])
${_DOCMD_USER_BLOCK_0}
[comment]: \# ([[_DOCMD_USER_BLOCK_0_END]])


\# Testing

[comment]: \# ([[_DOCMD_USER_BLOCK_1_BEGIN]])
${_DOCMD_USER_BLOCK_1}
[comment]: \# ([[_DOCMD_USER_BLOCK_1_END]])


\# Generating Documentation

[comment]: \# ([[_DOCMD_USER_BLOCK_2_BEGIN]])
${_DOCMD_USER_BLOCK_2}
[comment]: \# ([[_DOCMD_USER_BLOCK_2_END]])

[comment]: \# ([[_DOCMD_USER_BLOCK_3_BEGIN]])
${_DOCMD_USER_BLOCK_3}
[comment]: \# ([[_DOCMD_USER_BLOCK_3_END]])

$footer