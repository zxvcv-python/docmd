> \#\# $name
> ***
> $short_description
>
> $long_description
>
#if $parameters != ""
> \#\#\# <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
#for $param in $parameters
> |$param['name']|$param['type']|$param['direction']|$param['description']|
#end for
#end if
#if $return_type != "void"
> \#\#\# <u><i>Return:</i></u>
> ($return_type) $return_description
#end if
#if $return_values
> |CODE|DESCRIPTION|
> |-|-|
#for $ret_val in $return_values
> |$ret_val['value']|$ret_val['description']|
#end for
#end if

</br>

