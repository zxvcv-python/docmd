$header

\# ${name}

$description

[comment]: \# ([[_DOCMD_USER_BLOCK_0_BEGIN]])
${_DOCMD_USER_BLOCK_0}
[comment]: \# ([[_DOCMD_USER_BLOCK_0_END]])

***

[comment]: \# ([[_DOCMD_USER_BLOCK_1_BEGIN]])
${_DOCMD_USER_BLOCK_1}
[comment]: \# ([[_DOCMD_USER_BLOCK_1_END]])

</br>

\#\# Table of contents
***
#if $conan_package_doc
- [Package Settings](${docs_dir}/settings.md) - parameters, settings and defines and requirements description#end if
- [API reference](${docs_dir}/api.md) - api description for public interface
- [Building and Testing](${docs_dir}/build_test.md) - how to build and test package
- [Usage examples](${docs_dir}/examples.md)
- [Known Bugs](${docs_dir}/bugs.md)

</br>

\#\# License informations
***
$license

[comment]: \# ([[_DOCMD_USER_BLOCK_2_BEGIN]])
${_DOCMD_USER_BLOCK_2}
[comment]: \# ([[_DOCMD_USER_BLOCK_2_END]])

</br>

\#\# Authors
***
- $author

[comment]: \# ([[_DOCMD_USER_BLOCK_3_BEGIN]])
${_DOCMD_USER_BLOCK_3}
[comment]: \# ([[_DOCMD_USER_BLOCK_3_END]])

</br>

$footer
