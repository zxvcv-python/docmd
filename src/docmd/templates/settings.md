$header

\# Settings

#if $settings
#for $setting in $settings
- $setting
#end for
#end if

[comment]: \# ([[_DOCMD_USER_BLOCK_0_BEGIN]])
${_DOCMD_USER_BLOCK_0}
[comment]: \# ([[_DOCMD_USER_BLOCK_0_END]])

</br>

\# Options

#if $options
|NAME|AVAILABLE VALUES|DEFAULT VALUE|
|-|-|-|
#for $option in $options
|${option['name']}|${option['values']}|${option['default']}|
#end for
#end if

[comment]: \# ([[_DOCMD_USER_BLOCK_1_BEGIN]])
${_DOCMD_USER_BLOCK_1}
[comment]: \# ([[_DOCMD_USER_BLOCK_1_END]])

</br>

\# Requires

#if $requires
#for $require in $requires
- $require
#end for
#end if

[comment]: \# ([[_DOCMD_USER_BLOCK_2_BEGIN]])
${_DOCMD_USER_BLOCK_2}
[comment]: \# ([[_DOCMD_USER_BLOCK_2_END]])

$footer