from Cheetah.Template import Template

import pycparser_util.c_objects

from .CDoc import CDoc


class CFuncDecl(CDoc, pycparser_util.c_objects.CFuncDecl):
    def generate_doc_page(self) -> str:
        cdoc = self.get_cdoc(attribute_sensitive=True)

        if not cdoc:
            raise ValueError(f"Documentation comment not found for {self.name}")
        comment_by_lines = CDoc.strip_multiline_comment(cdoc)
        params = list(self.params)

        # parsing
        long_description = ""
        for i in range(1, len(comment_by_lines)):
            temp_line = comment_by_lines[i].strip()
            if temp_line.startswith("@"):
                break
            long_description += temp_line

        parameters = []
        param_coutner = 0
        for p in comment_by_lines:
            if not p.startswith("@param"):
                # if not empty - continuation of previous parameter doc.
                if parameters:
                    if not p.strip() or p.startswith("@"):
                        break
                    parameters[param_coutner - 1]["description"] += p.strip()
            else:
                temp = p.replace("@param", "").strip().split(" ", 2)
                parameters.append({
                    "name": temp[0],
                    "direction": temp[1][1:-1],
                    "description": temp[2]
                })
                if parameters[param_coutner]["name"] == params[param_coutner].name:
                    parameters[param_coutner]["type"] = params[param_coutner].type_str
                else:
                    raise ValueError("Documentation comment parameter dont match real declared parameters.")
                param_coutner += 1

        return_description = None
        values_counter = 0
        for p in comment_by_lines:
            values_counter += 1
            if p.startswith("@return"):
                return_description = p.replace("@return", "").strip()
                break

        return_values = []
        for l in range(values_counter, len(comment_by_lines) - 1):
            temp = comment_by_lines[l].split(" ", 1)
            return_values.append({
                "value": temp[0],
                "description": temp[1]
            })

        # generating
        return str(Template(
            self.load_template("api_fdecl"),
            [{
                "name": self.name,
                "short_description": comment_by_lines[0],
                "long_description": long_description,
                "parameters": parameters if parameters else "",
                "return_values": return_values,
                "return_type": self.type_str,
                "return_description": return_description
            }]
        ))

class CStruct(CDoc, pycparser_util.c_objects.CStruct):
    def generate_doc_page(self) -> str:
        comment_by_lines = CDoc.strip_multiline_comment(self.get_cdoc(attribute_sensitive=True))

        if not comment_by_lines:
            raise ValueError(f"Documentation comment not found for {self.name}")

        # parsing
        long_description = ""
        for i in range(1, len(comment_by_lines)):
            long_description += comment_by_lines[i].strip()

        members = []
        for m in self.members:
            member_cdoc = CDoc.strip_multiline_comment(m.get_cdoc(attribute_sensitive=True))
            members.append({
                "name": m.name,
                "type": m.type_str,
                "description": " ".join(member_cdoc) if member_cdoc else ""
            })

        # generating
        return str(Template(
            self.load_template("api_struct"),
            [{
                "name": self.name if self.name else "[anonymous]",
                "short_description": comment_by_lines[0],
                "long_description": long_description,
                "members": members
            }]
        ))

class CEnum(CDoc, pycparser_util.c_objects.CEnum):
    def generate_doc_page(self) -> str:
        comment_by_lines = CDoc.strip_multiline_comment(self.get_cdoc(attribute_sensitive=True))

        if not comment_by_lines:
            raise ValueError(f"Documentation comment not found for {self.name}")

        # parsing
        long_description = ""
        for i in range(1, len(comment_by_lines)):
            long_description += comment_by_lines[i].strip()

        items = []
        for item in self.values:
            member_cdoc = CDoc.strip_multiline_comment(item.get_cdoc(attribute_sensitive=True))
            items.append({
                "name": item.name,
                "val": item.value,
                "description": " ".join(member_cdoc) if member_cdoc else " "
            })

        # generating
        return str(Template(
            self.load_template("api_enum"),
            [{
                "name": self.name if self.name else "[anonymous]",
                "short_description": comment_by_lines[0],
                "long_description": long_description,
                "members": items
            }]
        ))

class CTypedef(CDoc, pycparser_util.c_objects.CTypedef):
    def generate_doc_page(self) -> str:
        comment_by_lines = CDoc.strip_multiline_comment(self.get_cdoc(attribute_sensitive=True))

        if not comment_by_lines:
            raise ValueError(f"Documentation comment not found for {self.name}")

        # parsing
        long_description = ""
        for i in range(1, len(comment_by_lines)):
            long_description += comment_by_lines[i].strip()

        # generating
        return str(Template(
            self.load_template("api_typedef"),
            [{
                "name": self.name,
                "short_description": comment_by_lines[0],
                "long_description": long_description,
                "definition": self.type.type.name
            }]
        ))
