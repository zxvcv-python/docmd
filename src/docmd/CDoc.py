from typing import List

from zxvcv.util.Path import here


class CDoc():
    @staticmethod
    def load_template(template_name:str) -> str:
        with (here(__file__)/f"templates/{template_name}.md").open("r") as t:
            return t.read()

    @staticmethod
    def strip_multiline_comment(comment:str) -> List[str]:
        if comment:
            return [l.replace("/**", "")
                     .replace("*/", "")
                     .replace("*", "")
                     .strip()
                    for l in comment.splitlines()]
        else:
            return comment
